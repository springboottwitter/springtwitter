package com.example.demo.controller;

import com.example.demo.model.dto.UserDto;

import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/adduser")
    public ModelAndView userView() {   // M and View to jest obiekt springa (1: jaki widok, 2: jaka nazwa obiektu 3: sam obiekt)
        return new ModelAndView("adduser", "userToInsert", new UserDto());
    }

//    ///////
//    @GetMapping("/users/edituser")
//    public ModelAndView editView() {   // M and View to jest obiekt springa (1: jaki widok, 2: jaka nazwa obiektu 3: sam obiekt)
//        return new ModelAndView("edituser", "userToInsert1", new UserDto());
//    }

    @PostMapping("/adduser")
    public String addUser(@ModelAttribute UserDto userDto) {
        userService.addUser(userDto);
        return "redirect:/adduser";

    }

    @GetMapping("/users")
    public ModelAndView getAllUsers() {
        return new ModelAndView("users", "userList", userService.getAllUsers());
    }

    @PostMapping("/deleteUser")
    public String deleteUser(@ModelAttribute("user") UserDto user) {
        userService.deleteUser(user.getId());
        return "redirect:/users";
    }

//    @RequestMapping(method = RequestMethod.PUT, value = "/users/edituser/{name}")
//    public User updateUserByName(@RequestBody User user, @PathVariable String name) {
//        return userService.updateUserByName(name,user);
//    }

//    @PutMapping("/users/edituser")
//    public User editUser(@RequestBody User user){
//        return userService.editUser(user);
//    }
}
