package com.example.demo.service;


import com.example.demo.model.dto.PostDto;
import com.example.demo.model.entity.Post;
import com.example.demo.model.entity.User;
import com.example.demo.repository.PostRepository;
import com.example.demo.repository.UserRepository;
import org.modelmapper.ModelMapper;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostService {

    private ModelMapper mapper;

    private PostRepository postRepository;
    private PasswordEncoder passwordEncoder;

    private UserRepository userRepository;

    public PostService(ModelMapper mapper, PostRepository postsRepository, UserRepository userRepository) {
        this.mapper = mapper;
        this.postRepository = postsRepository;
        this.userRepository = userRepository;
    }

    // dodawanie posta i przypisanie id usera
    public void addPost(PostDto postsDto) {
        Post posts = mapper.map(postsDto, Post.class);
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.findUserByLogin(login);
        posts.setUser(user);
        posts.setDate(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
        posts = postRepository.save(posts);
        System.out.println(posts);
    }

//    public void voteUp(PostDto postDto) {
//        Post post = mapper.map(postDto, Post.class);
//
//        post.setUpVote(post.getUpVote());
//        post = postRepository.save(post);
//    }


    // usuwanie posta dla admina, mozna usunac kazdego wybranego posta
    public void deletePost(Long id) {
        postRepository.deleteById(id);
    }

    // wyswietlenie wszystkich postów dla admina
    public List<PostDto> getAllPosts() {
        return postRepository.findAll().stream().map(u -> mapper.map(u, PostDto.class))
                .collect(Collectors.toList());
    }

    // wyswietlenie wszystkich postów na stronie głownej
    public List<PostDto> getAllNewPosts() {
        return postRepository.findAll().stream().map(u -> mapper.map(u, PostDto.class))
                .collect(Collectors.toList());
    }

    // wyszukanie jednego posta po ID zalogowanego usera
    public List<PostDto> getPostsByUserID(Long id) {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.findUserByLogin(login);
        id = user.getId();
        return postRepository.findAllByUserId(id)
                .stream().map(u -> mapper.map(u, PostDto.class)).collect(Collectors.toList());
    }


}


